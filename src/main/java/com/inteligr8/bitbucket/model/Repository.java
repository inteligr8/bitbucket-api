package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Repository {
	
	@JsonProperty(required = true)
	private Branch branch;
	
	public Branch getBranch() {
		if (this.branch == null)
			this.branch = new Branch();
		return this.branch;
	}

}
