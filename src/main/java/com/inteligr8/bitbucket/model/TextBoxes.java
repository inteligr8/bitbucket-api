package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextBoxes {
	
	private TextBox title;
	private TextBox description;
	private TextBox reason;
	
	public TextBox getTitle() {
		if (this.title == null)
			this.title = new TextBox();
		return this.title;
	}
	
	public TextBox getDescription() {
		if (this.description == null)
			this.description = new TextBox();
		return this.description;
	}
	
	public TextBox getReason() {
		if (this.reason == null)
			this.reason = new TextBox();
		return this.reason;
	}

}
