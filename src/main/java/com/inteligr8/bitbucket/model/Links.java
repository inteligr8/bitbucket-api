package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {
	
	private Link self;
	private Link html;
	
	public Link getSelf() {
		if (this.self == null)
			this.self = new Link();
		return this.self;
	}
	
	public Link getHtml() {
		if (this.html == null)
			this.html = new Link();
		return this.html;
	}

}
