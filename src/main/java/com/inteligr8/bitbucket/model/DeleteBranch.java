package com.inteligr8.bitbucket.model;

public class DeleteBranch {
	
	private DeleteBranch() {
	}
	
	public static String constructRequestPath(String repoName, String branchName) {
		return "/repositories/" + repoName + "/" + httpPath + "/" + branchName;
	}
	
	public static String httpPath = "refs/branches";

}
