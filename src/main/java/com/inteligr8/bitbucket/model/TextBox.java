package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextBox {
	
	private String raw;
	private Markup markup;
	private String html;
	
	public String getRaw() {
		return this.raw;
	}
	
	public void setRaw(String raw) {
		this.raw = raw;
	}
	
	public Markup getMarkup() {
		return this.markup;
	}
	
	public void setMarkup(Markup markup) {
		this.markup = markup;
	}
	
	public String getHtml() {
		return this.html;
	}
	
	public void setHtml(String html) {
		this.html = html;
	}
	
	public enum Markup {markdown, creole, plaintext};

}
