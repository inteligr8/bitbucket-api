package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.bitbucket.http.BaseResponse;

public class CreatePullRequest {
	
	private CreatePullRequest() {
	}
	
	public static String constructRequestPath(String repoName) {
		return "/repositories/" + repoName + "/" + httpPath;
	}
	
	public static String httpPath = "pullrequests";

	@JsonInclude(value = Include.NON_EMPTY)
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Request {
		
		@JsonProperty(required = true)
		private String title;
		private String description;
		@JsonProperty(required = true)
		private Repository source;
		private Repository destination;
		
		public String getTitle() {
			return title;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}
		
		public String getDescription() {
			return this.description;
		}
		
		public void setDescription(String description) {
			this.description = description;
		}
		
		public Repository getSource() {
			if (this.source == null)
				this.source = new Repository();
			return this.source;
		}
		
		public Repository getDestination() {
			if (this.destination == null)
				this.destination = new Repository();
			return this.destination;
		}
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Response extends BaseResponse {
		
		private int id;
		private String title;
		private State state;
		private Links links;
		
		public int getId() {
			return this.id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public String getTitle() {
			return this.title;
		}
		
		public void setTitle(String title) {
			this.title = title;
		}
		
		public State getState() {
			return this.state;
		}
		
		public void setState(State state) {
			this.state = state;
		}
		
		public Links getLinks() {
			if (this.links == null)
				this.links = new Links();
			return this.links;
		}
		
		public enum State {MERGED, SUPERSEDED, OPEN, DECLINED};

	}
	
}
