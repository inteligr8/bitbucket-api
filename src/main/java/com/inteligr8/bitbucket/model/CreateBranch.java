package com.inteligr8.bitbucket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.bitbucket.http.BaseResponse;

public class CreateBranch {
	
	private CreateBranch() {
	}
	
	public static String constructRequestPath(String repoName) {
		return "/repositories/" + repoName + "/" + httpPath;
	}
	
	public static String httpPath = "refs/branches";
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Request {
		
		@JsonProperty(required = true)
		private String name;
		@JsonProperty(required = true)
		private RequestTarget target;
		
		public String getName() {
			return this.name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public RequestTarget getTarget() {
			if (this.target == null)
				this.target = new RequestTarget();
			return this.target;
		}
		
		@JsonIgnoreProperties(ignoreUnknown = true)
		public class RequestTarget {
			
			@JsonProperty(required = true)
			private String hash;
			
			public String getHash() {
				return this.hash;
			}
			
			public void setHash(String hash) {
				this.hash = hash;
			}
		
		}
	
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Response extends BaseResponse {
		
	}

}
