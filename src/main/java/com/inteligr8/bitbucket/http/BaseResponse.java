package com.inteligr8.bitbucket.http;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.inteligr8.bitbucket.model.Error;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
	
	private int httpStatusCode;
	private String httpStatusReason;
	private String type;
	private Error error;
	
	public int getHttpStatusCode() {
		return this.httpStatusCode;
	}
	
	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
	public String getHttpStatusReason() {
		return this.httpStatusReason;
	}
	
	public void setHttpStatusReason(String httpStatusReason) {
		this.httpStatusReason = httpStatusReason;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Error getError() {
		if (this.error == null)
			this.error = new Error();
		return this.error;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class Data {
		
	}

}
