package com.inteligr8.bitbucket;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inteligr8.bitbucket.http.BaseResponse;
import com.inteligr8.bitbucket.http.PreemptiveAuthInterceptor;

public class ApiGateway {

	private final Logger logger = LoggerFactory.getLogger(ApiGateway.class);
	private final String baseUrl = "https://api.bitbucket.org/2.0";
	private ObjectMapper omapper = new ObjectMapper();
	private CredentialsProvider credProvider;
	
	public ApiGateway(CredentialsProvider credProvider) {
		this.credProvider = credProvider;
	}
	
	public <Response extends BaseResponse> Response get(String uriPath, Map<String, Object> paramMap, Class<Response> responseType) throws IOException {
		return this.execute(HttpGet.METHOD_NAME, uriPath, paramMap, null, responseType);
	}
	
	public <Request, Response extends BaseResponse> Response post(String uriPath, Request requestObject, Class<Response> responseType) throws IOException {
		return this.execute(HttpPost.METHOD_NAME, uriPath, null, requestObject, responseType);
	}
	
	public <Request, Response extends BaseResponse> Response put(String uriPath, Request requestObject, Class<Response> responseType) throws IOException {
		return this.execute(HttpPost.METHOD_NAME, uriPath, null, requestObject, responseType);
	}
	
	public <Response extends BaseResponse> Response delete(String uriPath, Map<String, Object> paramMap, Class<Response> responseType) throws IOException {
		return this.execute(HttpDelete.METHOD_NAME, uriPath, paramMap, null, responseType);
	}
	
	private <Request, Response extends BaseResponse> Response execute(String method, String uriPath, Map<String, Object> paramMap, Request requestObject, Class<Response> responseType) throws IOException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("execute('" + method + "', '" + uriPath + "')");

		RequestBuilder builder = RequestBuilder
				.create(method)
				.setUri(this.baseUrl + uriPath);
		
		if (paramMap != null) {
			for (Entry<String, Object> param : paramMap.entrySet())
				if (param.getValue() != null)
					builder.addParameter(param.getKey(), param.getValue().toString());
		}
		
		if (requestObject != null) {
			String requestJson = this.omapper.writeValueAsString(requestObject);
			if (this.logger.isTraceEnabled())
				this.logger.trace("execute('" + method + "', '" + uriPath + "'): " + requestJson);

			builder.setEntity(new StringEntity(requestJson, ContentType.APPLICATION_JSON));
		}
		
		HttpUriRequest request = builder.build();
		if (this.logger.isDebugEnabled())
			this.logger.debug("Prepared request for " + method + " to: " + uriPath);
		
		HttpResponse response = HttpClientBuilder
				.create()
				.addInterceptorFirst(new PreemptiveAuthInterceptor())
				.setDefaultCredentialsProvider(this.credProvider)
				.build()
				.execute(request);
		if (this.logger.isDebugEnabled())
			this.logger.debug("Received response from " + method + ": " + response.getStatusLine().getStatusCode());

		Response responseObject = null;
		if (response.getEntity() != null) {
			InputStream istream = response.getEntity().getContent();
			try {
				responseObject = this.omapper.readerFor(responseType).readValue(istream);
			} finally {
				istream.close();
			}
		} else {
			responseObject = this.omapper.readerFor(responseType).readValue("{}");
		}

		responseObject.setHttpStatusCode(response.getStatusLine().getStatusCode());
		responseObject.setHttpStatusReason(response.getStatusLine().getReasonPhrase());
		return responseObject;
	}

}
